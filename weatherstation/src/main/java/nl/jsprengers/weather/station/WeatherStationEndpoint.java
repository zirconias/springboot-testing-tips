package nl.jsprengers.weather.station;

import nl.jsprengers.weather.api.TemperatureUnit;
import nl.jsprengers.weather.api.WeatherReportDTO;
import nl.jsprengers.weather.api.WeatherStation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WeatherStationEndpoint implements WeatherStation {

    @Value("${temperature:42}")
    private double temperature;

    @Value("${humidity:80}")
    private int humidity;


    @RequestMapping(value = "weather", method = RequestMethod.GET)
    public WeatherReportDTO getWeatherReport() {
        return new WeatherReportDTO(temperature, TemperatureUnit.C, humidity);
    }
}
